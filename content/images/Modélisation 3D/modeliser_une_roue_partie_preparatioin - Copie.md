Title: Modélisation en 3D d'une Roue : Préparation
Date: 2019-02-11 13:09
Tags: logiciel, CAO, 3D, imprimante 3D, iRoboTechArt
Category: Modélisation 3D
Authors: Victor Lohézic
Summary: L'objectif est de modéliser la roue d'un robot en 3D. Commençons par calculer sa taille.
Image: images/technologies/irobotechart_logo_openscad/logo.png
Status: draft

<script>

  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
    </script>
    
Notre objectif est de modéliser en 3D une roue pour un robot par exemple. Mais avant d'utiliser le logiciel, faisons le point sur les informations que nous disposons.

  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel teal">
          <span class="card-title white-text"><b>Information 1</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">On part du principe que notre robot dispose du motoréducteur suivant :
            <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_preparation/datasheet.png" alt="Documentation technique : 33rpm"></center>
        </span>
      </div>
    </div>
  </div>
    <div class="row">
    <div class="col s12 m12">
      <div class="card-panel teal">
          <span class="card-title white-text"><b>Mission</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Voici votre première mission, on veut que notre robot doit se déplacer à une vitesse de 0.17m/s. Quelle doit être le rayon de notre roue ? 
        </span>
      </div>
    </div>
  </div>
  <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">lightbulb_outline</i>Indice 1</div>
      <div class="collapsible-body">
          Convertir la vitesse angulaire de la documentation technique en rad/s.
        </div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">lightbulb_outline</i>Indice 2</div>
      <div class="collapsible-body">
          Relation entre vitesse d'un point et vitesse angulaire :
          <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_preparation/vitesse-lineaire.png" alt="V = W * r"></center>
        </div>
    </li>
    <li>
      <div class="collapsible-header"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body">
          On lit sur la documentation technique que la vitesse de rotation est de 33 rpm (rotation par minute) soit 33 tr/min. 
          On convertit la vitesse de rotation en rad/s :
          <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_preparation/conversion_rotation_angulaire.png" alt="Formule de la conversion"></center>
          <br>
          <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_preparation/calcul_conversion_vitesse_angulaire.png" alt="Application numérique"></center>
          Maintenant on déduit la rayon grâce à la relation suivante :
          <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_preparation/vitesse-lineaire.png" alt="V = W * r">
          </center>
          L'application numérique est donc la suivante :
          <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_preparation/calcul_rayon.png" alt="Application numérique">
          </center>
          Le rayon de notre roue sera donc de 5 cm.
        </div>
    </li>
  </ul>
    <div class="row">
    <div class="col s12 m12">
      <div class="card-panel teal">
          <span class="card-title white-text"><b>Information 2</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Pour améliorer l'adhérence on mettra un élastique autour de notre roue de largeur 1.8 mm.
        </span>
      </div>
    </div>
  </div>
  
Dans les prochains articles, nous modéliserons la roue avec trois logiciels de modélisations différents :
     <table>
        <thead>
          <tr>
              <th><img class="responsive-img" width="60%" src="images/modelisation_3D/roue_preparation/openscad.png" alt="Logo OpenScad"></th>
              <th><img class="responsive-img" width="20%" src="images/modelisation_3D/roue_preparation/freecad.png" alt="Logo FreeCAD"></th>
              <th><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_preparation/tinkercad.png" alt="Logo Tinkercad"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>[OpenScad](http://www.openscad.org/)</td>
            <td>[FreeCAD](https://www.freecadweb.org/)</td>
            <td>[Tinkercad](https://www.tinkercad.com/)</td>
          </tr>
        </tbody>
      </table>



