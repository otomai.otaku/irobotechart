Title: Modélisation en 3D d'une Roue : OpenScad
Date: 2019-02-2 13:09
Tags: logiciel, CAO, 3D, imprimante 3D, iRoboTechArt
Category: Modélisation 3D
Authors: Victor Lohézic
Summary: L'objectif est de modéliser la roue d'un robot en 3D. Commençons par calculer sa taille.
Image: images/technologies/irobotechart_logo_openscad/logo.png
Status: draft

<script>

  $(document).ready(function(){
    $('.collapsible').collapsible();
  });
    </script>
    
Nous allons modéliser avec le logciel OpenScad la roue que nous avons préparé dans ce précédent article :
Pour réaliser les différentes missions, ne négliger la feuille de rapel :

  <div class="row">
    <div class="col s12 m12">
      <div class="card-panel teal">
          <span class="card-title white-text"><b>Objectif</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Voici la roue que nous devons réaliser :
            <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_openscad/final.png" alt="Image de la roue modélisée"></center>
        </span>
      </div>
    </div>
  </div>
    <div class="row">
    <div class="col s12 m12">
      <div class="card-panel teal">
          <span class="card-title white-text"><b>Mission 1</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Commencez par réaliser un cylindre avec un diamètre de 5cm (Attention les valeurs sont en mm dans OpenScad) et une hauteur de 2mm :
            <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_openscad/cercle1.png" alt="cylindre avec un diamètre de 5cm"></center>
        </span>
      </div>
    </div>
  </div>
  <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body">
          <code>
            epaisseur = 2;<br><br>
            module cercle1(){<br>
                cylinder(epaisseur,d=50,$fn=100); 
            }<br><br>
            cercle1();<br>
          </code>
          $fn permet de régler la résolution du cylindre
        </div>
    </li>
  </ul>

<div class="row">
    <div class="col s12 m12">
      <div class="card-panel teal">
          <span class="card-title white-text"><b>Mission 2</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Réalisez un second cylindre avec un diamètre de 48mm  et une hauteur de 1.8 mm pour pouvoir loger l'élastique :
            <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_openscad/cercle2.png" alt="cylindre avec un diamètre de 48mm  et une hauteur de 1.8 mm"></center>
        </span>
      </div>
    </div>
  </div>
  <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body">
          <code>
            module cercle2(){<br>
                cylinder(1.8,d=47,$fn=100); <br>
            }<br><br>
            cercle2();<br>
          </code>
        </div>
    </li>
  </ul>
    
<div class="row">
    <div class="col s12 m12">
      <div class="card-panel teal">
          <span class="card-title white-text"><b>Mission 3</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Réalisez un dernier cylindre avec un diamètre de 5cm  et une hauteur de 2 mm :
            <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_openscad/roue.png" alt="Roue non percée"></center>
        </span>
      </div>
    </div>
  </div>
  <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body">
          <code>
            epaisseur = 2;<br><br>
            module cercle1(){<br>
                cylinder(epaisseur,d=50,$fn=100); <br>
            }<br><br>
            module cercle2(){<br>
                cylinder(1.8,d=47,$fn=100); <br>
            }<br><br>
            module roue(){<br>
                cercle1();<br>
                translate([0,0,epaisseur]){<br>
                   cercle2();<br>
                }<br>
                translate([0,0,epaisseur+1.8]){<br>
                    cercle1();<br>
                }<br>
            } <br><br>
            roue();
          </code>
        </div>
    </li>
  </ul>

<div class="row">
    <div class="col s12 m12">
      <div class="card-panel teal">
          <span class="card-title white-text"><b>Mission 4</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Nous allons préparer le trou. Pour cela, il faut modéliser l'axe du motoréducteur cf lien documentation. Dessiner un cylindre de diamètre 4 mm et de hauteur 5.8 mm de hauteur. Ensuite deplacer un cube de telle sorte à enlever de la matière à notre cylindre :
            <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_openscad/cylindre.png" alt="cylindre"></center>
            <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_openscad/cube.png" alt="cube"></center>
            <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_openscad/axe.png" alt="axe"></center>
        </span>
      </div>
    </div>
  </div>
  <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body">
          <code>
            module axe(){
                difference(){
                    cylinder(epaisseur*2+1.8,d=4,$fn=100); 
                    translate([-2,1.5,0]){
                        cube([4,4,epaisseur*2+1.8]);
                        }
                    }
                }
            axe();
          </code>
        </div>
    </li>
  </ul>
<div class="row">
    <div class="col s12 m12">
      <div class="card-panel teal">
          <span class="card-title white-text"><b>Mission 4</b>
                        <img align="right" class="responsive-img" width="5%" src="images/technologies/irobotechart_logo_openscad/character.png" alt="Personnage">
        </span>
        <span class="white-text">Faîtes un trou dans la roue de la forme et taille de l'axe modélisée précedemment. 
            <center><br><img class="responsive-img" width="50%" src="images/modelisation_3D/roue_openscad/final.png" alt="Roue"></center>
        </span>
      </div>
    </div>
  </div>
  <ul class="collapsible">
    <li>
      <div class="collapsible-header"><i class="material-icons">vpn_key</i>Solution</div>
      <div class="collapsible-body">
          <code>
            epaisseur = 2;<br>
            module cercle1(){<br>
                cylinder(epaisseur,d=50,$fn=100); <br>
            }<br>
            module cercle2(){
                cylinder(1.8,d=47,$fn=100); <br>
            }<br>
<br>
            module axe(){<br>
                difference(){<br>
                    cylinder(epaisseur*2+1.8,d=4,$fn=100);<br> 
                    translate([-2,1.5,0]){<br>
                        cube([4,4,epaisseur*2+1.8]);<br>
                        }<br>
                    }<br>
                }<br>
<br>
            module roue(){<br>
                cercle1();<br>
                translate([0,0,epaisseur]){<br>
                   cercle2();<br>
                }<br>
                translate([0,0,epaisseur+1.8]){<br>
                    cercle1();<br>
                }<br>
            }    <br>
<br>
            difference(){<br>
                roue();<br>
                axe();<br>
            }<br>
          </code>
        </div>
    </li>
  </ul>

