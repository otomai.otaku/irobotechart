Title: Nouvelle Interface Framapad
Date: 2019-04-30 17:00
Tags:  
Category: Libre
Authors: Victor Lohézic
Summary: Plus aérée et jolie, le nouvelle version d'Etherpad (v1.7.5) mise en ligne sur les serveurs Framasoft est une bonne nouvelle.
Image: images/libre/framapad_nouvelle_interface/thumbnail.PNG
Status: published

Tout d'abord, Framapad est un <b>éditeur de texte en ligne</b>. C'est un outil <b>libre et collaboratif</b> très intéressant qui se présente comme une alternative à "Google Doc". Ce projet s'inscrit donc dans la campagne de Framasoft ["Dégooglisons Internet"](https://degooglisons-internet.org/fr/alternatives)
Framapad n’est pas développé par Framasoft. En effet, c'est une instance (une installation sur un serveur) du logiciel libre [Etherpad](https://etherpad.org/).
<br>
Le principal inconvénient de cet outil était son design qui pouvait dissuader de l'utiliser. En effet, certains outils libres présentent un service de qualité mais une interface peu attirante. Plus aérée et jolie, le nouvelle version d'Etherpad (v1.7.5) mise en ligne sur les serveurs Framasoft est une <b>bonne nouvelle</b>. Je vous invite à la tester [ici](https://framapad.org/fr/).
<br>
<br>
<center>
    <img class="responsive-img" src="https://irobotechart.com/images/libre/framapad_nouvelle_interface/framapad.PNG" alt="Image nouvelle version Framapad">
</center>
<br>
<br>
L'éditeur de texte est très agréable à prendre en main. Il est <b>simple et efficace</b>. Si vous voulez en savoir plus sur Framapad, vous pouvez <b>regarder cet article</b> : [https://framablog.org/2019/04/29/des-framapads-plus-beaux-grace-a-une-belle-contribution/](https://framablog.org/2019/04/29/des-framapads-plus-beaux-grace-a-une-belle-contribution/)<br>
Je le trouve très bien écrit puisqu'il insiste sur les <b>contributions</b> qui ont permis l'existence de cette mise à jour. 




